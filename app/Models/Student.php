<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class Student extends Model
{
    protected $table = 'students';
    protected $fillable = ['name', 'email', 'phone'];
    protected $dates = ['deleted_at'];

    protected $softDelete = true;
}
