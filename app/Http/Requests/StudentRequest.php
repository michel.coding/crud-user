<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome deve ser preenchido.',
            'email.required' => 'E-mail deve ser preenchida.',
            'phone.required' => 'Telefone deve ser númerico.',
            'email.unique' => 'E-mail já foi cadastrado.',
        ];
    }
}
