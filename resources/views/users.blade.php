@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Listar usuário</div>

                <div class="panel-body">
                    <div class="bs-example" data-example-id="contextual-table">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nome</th>
                              <th>E-mail</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="active">
                          @foreach($users as $user)
                              <th scope="row">{{ $user->id }}</th>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>
                                <a   href="{{route('users.edit',$user->id)}}"class="btn btn-secondary">Editar </a>

                              </td>
                           @endforeach   
                            </tr>
                           
                          </tbody>
                        </table>
                      </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
