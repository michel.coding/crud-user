@extends('layout')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
  <a href="{{ route('students.create')}}" class="btn btn-primary btn-sm">Criar Usuário</a><br>
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <br>
  <table class="table">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Nome</td>
          <td>E-mail</td>
          <td>Telefone</td>
          
          <td class="text-center">Ações</td>
        </tr>
    </thead>
    <tbody>
        @foreach($student as $students)
        <tr>
            <td>{{$students->id}}</td>
            <td>{{$students->name}}</td>
            <td>{{$students->email}}</td>
            <td>{{$students->phone}}</td>
           
            <td class="text-center">
                <a href="{{ route('students.edit', $students->id)}}" class="btn btn-primary btn-sm">Editar</a>
                <form action="{{ route('students.destroy', $students->id)}}" method="post" style="display: inline-block">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="DELETE" >

                    <button class="btn btn-danger btn-sm" alue="delete " type="submit">Deletar</button>
                  </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection