@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <a href="{{ route('students.create')}}" class="btn btn-primary btn-sm">Criar Usuário</a><br>
            </div>
        </div>
    </div>
</div>
@endsection
